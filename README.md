# MITx_6.86x - Machine Learning with Python: from Linear Models to Deep Learning


## Student's notes (2020 run) ##

_Disclaimer: The following notes are a mesh of my own notes, selected transcripts, some useful forum threads and various course material. I do not claim any authorship of these notes, but at the same time any error could well be arising from my own interpretation of the material._

- [Unit 00 - Course Overview, Homework 0, Project 0](Unit 00 - Course Overview, Homework 0, Project 0/Unit 00 - Course Overview, Homework 0, Project 0.md)

- [Unit 01 - Linear Classifiers and Generalizations](Unit 01 - Linear Classifiers and Generalizations/Unit 01 - Linear Classifiers and Generalizations.md)

- [Unit 02 - Nonlinear Classification, Linear regression, Collaborative Filtering](Unit 02 - Nonlinear Classification, Linear regression, Collaborative Filtering/Unit 02 - Nonlinear Classification, Linear regression, Collaborative Filtering)
